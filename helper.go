package hdkeys

import (
	"bytes"
	"crypto/hmac"
	"crypto/rand"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/hex"
	"errors"
	"fmt"
	"io"
	"log"
	"math/big"
	"strconv"
	"strings"
	"unicode"

	"github.com/sirupsen/logrus"
	"golang.org/x/crypto/ripemd160"
)

// xKeyValidate validate a extended key string
// and return its path and key if validation is passed
func xKeyValidate(s string) ([]byte, string) {
	// decode xprv string
	x, err := base58Dec(s)
	if err != nil {
		logrus.Fatal("could not decode xpub string")
	}

	// check length
	if len(x) != 82 {
		logrus.Fatal("length of decoded xpub is not 82 bytes")
	}

	// check checksum to see if it is valid xpub
	k, cs := x[:82-4], x[82-4:82]
	if !bytes.Equal(doubleSHA256(k, 4), cs) {
		logrus.Fatal("could not match xpub check sum with the xpub key")
	}

	//check depth
	depth := x[4:5][0]
	p := "M"
	if depth != byte(0x00) {
		p = func() string {
			s := "M"
			for i := 0; i < int(depth); i++ {
				s += "/N" + string(48+i+1)
			}
			return s
		}()
	}

	return x, p
}

// exand return expanded x,y point of the public key
// when key from xpub is provided
// https://crypto.stackexchange.com/a/8916
func expand(key []byte) (*big.Int, *big.Int) {
	params := curve.Params()
	exp := big.NewInt(1)
	exp.Add(params.P, exp)
	exp.Div(exp, big.NewInt(4))
	x := big.NewInt(0).SetBytes(key[1:33])
	y := big.NewInt(0).SetBytes(key[:1])
	beta := big.NewInt(0)
	beta.Exp(x, big.NewInt(3), nil)
	beta.Add(beta, big.NewInt(7))
	beta.Exp(beta, exp, params.P)
	if y.Add(beta, y).Mod(y, big.NewInt(2)).Int64() == 0 {
		y = beta
	} else {
		y = beta.Sub(params.P, beta)
	}
	return x, y
}

func decodeVerbytes(b []byte) (net, ver string) {
	v, ok := verByteRevMap[fmt.Sprintf("%X", b)]
	if !ok {
		logrus.Fatal("could not get correct verion from the ext key")
	}
	sv := strings.Split(v, "-")

	return sv[0], sv[1]
}

// returns the coordinate pair resulting from EC point
// multiplication (repeated application of the EC group operation)
// of the secp256k1 base point with the integer p
func point(p []byte) (*big.Int, *big.Int) {
	x, y := curve.ScalarBaseMult(p)
	return x, y
}

// serializes the coordinate pair P = (x,y) as a byte sequence
// using SEC1's compressed form: (0x02 or 0x03) || ser256(x),
// where the header byte depends on the parity of the omitted y coordinate.
func serP(x, y *big.Int) []byte {
	even, _ := hex.DecodeString("02")
	odds, _ := hex.DecodeString("03")
	key33 := make([]byte, 0, 33)

	// append parity byte
	if y.Bit(0) == 0 {
		key33 = append(key33, even...)
	} else {
		key33 = append(key33, odds...)
	}
	return append(key33, ser256(x)...)
}

// serialize a 32-bit unsigned integer i as a 4-byte sequence,
// most significant byte first
func ser32(i *big.Int) []byte {
	b := i.Bytes()

	if len(b) < 4 {
		leadZero := make([]byte, 4-len(b))
		b = append(leadZero, b...)
	}
	return b[:4]

}

// serializes the integer p as a 32-byte sequence,
// most significant byte first
func ser256(p *big.Int) []byte {
	pb := p.Bytes()

	// padding with leading zero
	for lz := len(pb); lz < 32; lz++ {
		pb = append(oneZeroByte, pb...)
	}
	return pb[:32]
}

// Interprets a 32-byte sequence as a 256-bit number,
//  most significant byte first.
func parse256(p []byte) *big.Int {
	bp := big.NewInt(0)
	return bp.SetBytes(p)
}

// Hash160 (RIPEMD160 after SHA256) of pub key K
func hash160(data []byte) []byte {
	sha := sha256.New()
	ripe := ripemd160.New()
	sha.Write(data)
	ripe.Write(sha.Sum(nil))
	return ripe.Sum(nil)
}

// wrapper func for validatePrivatekey,
// easier for testing purpose
func prvValidate(k []byte) []byte {
	if err := validatePrivateKey(k); err != nil {
		log.Fatalf("private key invalid, %s ", err)
	}
	return k
}

// validatePrivateKey check if private key is not in compliance
// with the length, value , and non zero requirement
func validatePrivateKey(key []byte) error {

	kb := big.NewInt(0).SetBytes(key)
	zeroBool := strings.ContainsAny(fmt.Sprintf("%b", kb), "1")

	if !zeroBool {
		return errors.New("could not validate pkey - key is zero value")
	}

	if bytes.Compare(key, curve.N.Bytes()) >= 0 {
		return errors.New("could not validate private Key, out side curve")
	}

	if len(key) != 32 {
		return errors.New("could not validate private Key, length not correct")
	}
	return nil
}

func idxDecode(s string) (*big.Int, error) {
	// child nodes (depth >= 1), limit to 4 level
	var num int64
	var hBool = false
	var lastR = rune(s[len(s)-1])
	var firstR = rune(s[0])

	// length too long , longest '256H'
	if len(s) > 4 {
		return nil, errors.New("could not decode child index, len > 4")
	}

	// first letter is not Number
	if !unicode.IsNumber(firstR) {
		return nil, errors.New("could not decode child index, 1st char is not number")
	}

	// check if last letter is H or h or '
	if unicode.IsLetter(lastR) {
		if !(string(lastR) == "H" || string(lastR) == "h") {
			return nil, errors.New("could not find [H/h] in last Rune")
		}
		hBool = true

		if hBool {
			s = s[:len(s)-1]
		}
	}

	// start conversion
	n, err := strconv.Atoi(s)
	if err != nil {
		return nil, err
	}
	num = int64(n)

	// hardened child
	cn := big.NewInt(num)
	if hBool {
		return cn.Add(harden, cn), nil
	}
	return cn, nil
}

func bytePlusOne(b []byte) []byte {
	bigb := big.NewInt(0).SetBytes(b)
	return bigb.Add(bigb, big.NewInt(1)).Bytes()
}

// version returns version bytes (len = 4)
func verBytes(p, q string) []byte {
	b := make([]byte, 4)
	v, ok := verByteMap[p+"-"+q]
	if !ok {
		logrus.Fatal("could not decode version bytes")
	}
	// must be 4 bytes from v, no error check here
	b, _ = hex.DecodeString(v)
	return b
}

// serializeBytes return a concatenated slice bytes
func serializeBytes(bs ...[]byte) []byte {
	var sb = make([]byte, 0, 78)
	for _, v := range bs {
		// fmt.Printf("%x\n", v)
		sb = append(sb, v...)
	}
	return sb
}

// doubleSHA256 return first l bytes of double SHA256 given
// a slice of bytes
func doubleSHA256(b []byte, l uint) []byte {
	cs1 := sha256.New()
	cs1.Write(b)
	cs2 := sha256.New()
	cs2.Write(cs1.Sum(nil))
	return cs2.Sum(nil)[:l]
}

// hm512 implement BIP32 master key generation step:
// HMAC-SHA512(Key = "Bitcoin seed", Data = S)
func hm512(secret, seed []byte) ([]byte, error) {
	hmac := hmac.New(sha512.New, secret)
	_, err := hmac.Write(seed) // sign with secret

	if err != nil {
		log.Fatalf("could not create I from func h512, %s", err)
	}

	return hmac.Sum(nil), err

}

// //https://play.golang.org/p/k-oGL7yrBnY
func base58Enc(b []byte) string {

	// encode
	es := make([]byte, 0, len(b)*maxlen/1000)
	b58 := big.NewInt(58)
	zero := big.NewInt(0)
	x := big.NewInt(0).SetBytes(b)
	mod := new(big.Int)

	for x.Cmp(zero) > 0 {
		x.DivMod(x, b58, mod)
		es = append([]byte{b58char[mod.Int64()]}, es...)
	}

	// add 1 to encoded string with leading 0
	leadZero := func(bs []byte) bool {
		for _, v := range bs {
			switch v {
			case 0x00:
				return true
			default:
				return false
			}
		}
		return false
	}

	if leadZero(b) {
		es = append([]byte{b58char[0]}, es...)

	}

	return fmt.Sprintf("%s", es)
}

func createCharIdxMap(s string) (map[byte]int64, error) {
	if len(s) != 58 {
		return nil, errors.New("could not crate index map, length err")
	}

	m := make(map[byte]int64)
	for i, b := range []byte(s) {
		m[b] = int64(i)
	}

	return m, nil
}

// base58Dec returns nil when invalid chars are found
func base58Dec(s string) ([]byte, error) {

	b := []byte(s)

	startIdx := 0
	buf := &bytes.Buffer{}

	for i, c := range b {
		if c == b58char[0] {
			if err := buf.WriteByte(0x00); err != nil {
				return nil, err
			}
		} else {
			startIdx = i
			break
		}
	}

	n := big.NewInt(0)
	div := big.NewInt(58)
	idxMap, err := createCharIdxMap(b58char)
	if err != nil {
		logrus.Fatal("could not create index map during decode b58 chars")
	}

	for _, c := range b[startIdx:] {
		charIdx, ok := idxMap[c]
		if !ok {
			return nil, errors.New("could not have valid char in decode string")
		}

		n.Add(n.Mul(n, div), big.NewInt(charIdx))
	}

	buf.Write(n.Bytes())

	return buf.Bytes(), nil
}

// GenKeyPairs generates key pairs based on secp256k1 ec curve
// definition (k = private in hex, p = address in compressed format)
func GenKeyPairs() (k *big.Int, p string) {
	k = genOnePrv()
	p = genOneCmpPub(k)
	return
}

func genOnePrv() *big.Int {

	// Make a Rand big number from RNG
	b := make([]byte, curve.N.BitLen()/8+8)

	_, err := io.ReadFull(rand.Reader, b)
	if err != nil {
		return nil
	}

	// mod by prime N ,
	// Private Key is k
	var one = new(big.Int).SetInt64(1)
	k := new(big.Int).SetBytes(b)
	n := new(big.Int).Sub(curve.N, one)
	k.Mod(k, n)
	k.Add(k, one)

	return k
}

func genOneCmpPub(k *big.Int) string {

	p := []byte{}

	// Gen Pub Key from EC
	pX, pY := point(k.Bytes())

	bX := pX.Bytes()

	padX := append(bytes.Repeat([]byte{0x00}, 32-len(bX)), bX...)

	/* Add prefix 0x02 or 0x03 depending on ylsb */
	if pY.Bit(0) == 0 {
		p = serializeBytes([]byte{0x02}, padX)
	} else {
		p = serializeBytes([]byte{0x03}, padX)
	}

	p = hash160(p)

	p = serializeBytes([]byte{0x00}, p)

	p = serializeBytes(p, doubleSHA256(p, 4))

	return base58Enc(p)
}
