// Package hdkeys implement BIP32 using GO
// Elliptic curve package is forked from btcsuite/btcd
package hdkeys // import "bitbucket.org/kccleoc/hdkeys"

import (
	"bytes"
	"math/big"

	"bitbucket.org/kccleoc/btcec"
	"github.com/sirupsen/logrus"
)

var (
	oneZeroByte   = make([]byte, 1)
	fourZeroByte  = make([]byte, 4)
	curve         = btcec.S256()
	harden        = new(big.Int).Lsh(big.NewInt(2), 31-1)
	b58char       = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz"
	verByteRevMap = map[string]string{
		"0488B21E": "main-public",
		"0488ADE4": "main-private",
		"043587CF": "test-public",
		"04358394": "test-private",
	}
	verByteMap = map[string]string{
		"main-public":  "0488B21E",
		"main-private": "0488ADE4",
		"test-public":  "043587CF",
		"test-private": "04358394",
	}
	//int(math.Abs(math.Log(256) / math.Log(58)))*1000
	maxlen = 1365
)

// Node define the BIP32 Node
type Node struct {
	net         string
	version     string
	depth       []byte
	fingerprint []byte
	childNumber []byte
	chain       []byte
	k           []byte
	Path        string
}

func setNode(net, version, path string,
	depth, fingerprint, childnumber, chain, k []byte) *Node {
	return &Node{
		net:         net,
		version:     version,
		depth:       depth,
		fingerprint: fingerprint,
		childNumber: childnumber,
		chain:       chain,
		k:           k,
		Path:        path,
	}
}

// NewWallet returns a new node (master node)
// with input seed bytes and network version s
// Secret "Bitcoin seed" as required with BIP32
func NewWallet(seed []byte, s string) *Node {
	I, _ := hm512([]byte("Bitcoin seed"), seed)
	return &Node{
		net:   s,
		chain: I[len(I)/2:],
		k:     prvValidate(I[:len(I)/2]),
	}
}

// RestoreXprv return a Node from xprv string
func RestoreXprv(s string) *Node {
	x, path := xKeyValidate(s)

	// find net / version of xprv
	n, v := decodeVerbytes(x[0:4])

	return &Node{
		net:         n,
		version:     v,
		depth:       x[4:5],
		fingerprint: x[5:9],
		childNumber: x[9:13],
		chain:       x[13:45],
		k:           x[46:78], // skip one 0x00 for xprv case
		Path:        path,
	}
}

// RestoreXpub return a Node from xpub string
func RestoreXpub(s string) *Node {
	x, path := xKeyValidate(s)

	// find net / version of xprv
	n, v := decodeVerbytes(x[0:4])

	return &Node{
		net:         n,
		version:     v,
		depth:       x[4:5],
		fingerprint: x[5:9],
		childNumber: x[9:13],
		chain:       x[13:45],
		k:           x[45:78],
		Path:        path,
	}
}

// N create Next level of node derived from parent
// with identifier id, id with "H" mean it is harden
// child & master node is denoded with "m" / "M"
func (n *Node) N(id string) *Node {

	switch id {
	// master node, the first node from the "seed" or
	//	from root xprv / xpub key
	case "m", "M":
		return setNode(n.net, "private", n.Path+id,
			oneZeroByte, fourZeroByte, fourZeroByte, n.chain, n.k)
	default:
		// child number
		i, err := idxDecode(id)
		if err != nil {
			logrus.Fatal("could not do decode of child number ")
		}

		// Key chain , Key , fingerprint
		nc, nk, fgp := n.newKey(i)

		return setNode(n.net, n.version, n.Path+"/"+id,
			bytePlusOne(n.depth), fgp, ser32(i), nc, nk)
	}
}

// newKey generates for current node the chain and key based on
// parent's chain code n.c and key n.k
// return also the fingerprint of key
func (n *Node) newKey(i *big.Int) (c []byte, k []byte, f []byte) {
	switch n.version {
	case "private":
		c, k = n.prvCKD(i)
		f = hash160(serP(point(n.k)))[:4]
		return
	case "public":
		c, k = n.pubCKD(i)
		f = hash160(n.k)[:4]
		return
	}
	logrus.Fatal("unknown version found, could not gen new key...")
	return nil, nil, nil
}

// prvCKD computes a child chain code and key from the parent prv key
//  harden child --> based on n.k
//  normal child --> based on point(n.k), i.e. pubkey G x N on ec curve
func (n *Node) prvCKD(i *big.Int) ([]byte, []byte) {
	// check if child number is harden, i.e. >= 2^31

	var I = make([]byte, 64)

	k := big.NewInt(0).SetBytes(n.k)

	switch i.Cmp(harden) {
	// harden child
	case 0, 1:

		// let I = HMAC-SHA512(Key = c par, Data = 0x00 || ser256(k par) || ser32(i))
		// (Note: The 0x00 pads the private key to make it 33 bytes long.)
		I, _ = hm512(n.chain, serializeBytes(oneZeroByte, ser256(k), ser32(i)))

	// normal child
	case -1:

		// let I = HMAC-SHA512(Key = c par, Data = serP(point(kpar)) || ser32(i))
		I, _ = hm512(n.chain, serializeBytes(serP(point(n.k)), ser32(i)))

		// fmt.Printf("I (prvCDK) : \t%x\n", I)

	}

	//split into two 32 bytes sequence
	//I(right) --> chain code for next node
	ci := I[32:]

	// ki is parse256(Left 32 byte of I) + kpar
	ki := big.NewInt(0).Add(parse256(I[:32]), k)

	// (mod n)
	ki.Mod(ki, curve.N)

	// check parse256(IL) â¥ n or ki = 0
	// (Note: this has probability of erro is lower than 1 in 2^127.)
	prvValidate(I[:32])

	return ci, ki.Bytes()
}

func (n *Node) pubCKD(i *big.Int) ([]byte, []byte) {
	var I = make([]byte, 64)

	switch i.Cmp(harden) {
	// harden child
	case 0, 1:
		logrus.Fatalf("hardend childs could not be derived by pub key")

	// normal child
	case -1:
		//let I = HMAC-SHA512(Key = cpar, Data = serP(Kpar) || ser32(i)).
		I, _ = hm512(n.chain, serializeBytes(n.k, ser32(i)))

		// fmt.Printf("I : (pub CDK) \t%x\n", I)
	}
	//split into two 32 bytes sequence
	//I(right) --> chain code for next node
	ci := I[32:]

	//  Ki is point(parse256(IL)) + Kpar.
	// "+" above mean EC add the two point k1 k2
	ki := func(k1, k2 []byte) []byte {
		x1, y1 := expand(k1)
		x2, y2 := expand(k2)
		curve.Add(x1, y1, x2, y2)
		return serP(curve.Add(x1, y1, x2, y2))
	}(serP(point(I[:32])), n.k)

	// check parse256(IL) â¥ n or ki = 0
	// (Note: this has probability of erro is lower than 1 in 2^127.)
	prvValidate(I[:32])

	return ci, ki
}

// Xprv return xprv key as []byte
func (n *Node) Xprv() []byte {

	if n.version == "private" {
		k := big.NewInt(0).SetBytes(n.k)

		x := serializeBytes(verBytes(n.net, n.version), n.depth, n.fingerprint,
			n.childNumber, n.chain, oneZeroByte, ser256(k))

		// check sum of 4 byte
		checksum := doubleSHA256(x, 4)

		return serializeBytes(x, checksum)

	}
	return nil
}

// XprvEnc58 returns base58 encoded xprv BIP32 key
func (n *Node) XprvEnc58() string {
	return base58Enc(n.Xprv())
}

// PriKeyWIF return a WIF format of pri key
func (n *Node) PriKeyWIF(compress bool) string {
	if n.version == "private" {
		// ref : https://en.bitcoin.it/wiki/Wallet_import_format
		prefix := byte(0x80) //main net
		if n.net == "test" {
			prefix = byte(0xEF)
		}

		compFlag := byte(0x01)

		payload := serializeBytes([]byte{prefix}, n.k)

		if compress {
			payload = serializeBytes(payload, []byte{compFlag})
		}

		p := serializeBytes(payload, doubleSHA256(payload, 4))

		logrus.Debugf("WIF in hex: %X", p)

		return base58Enc(p)
	}
	return ""
}

// Xpub return xpub key as []byte
func (n *Node) Xpub() []byte {

	var x = make([]byte, 0, 78)

	if n.version == "private" {

		x = serializeBytes(verBytes(n.net, "public"), n.depth, n.fingerprint,
			n.childNumber, n.chain, serP(point(n.k)))
	} else {
		x = serializeBytes(verBytes(n.net, "public"), n.depth, n.fingerprint,
			n.childNumber, n.chain, n.k)
	}

	// check sum of 4 byte
	checksum := doubleSHA256(x, 4)

	return serializeBytes(x, checksum)
}

// XpubEnc58 returns base58 encoded xpub BIP32 key
func (n *Node) XpubEnc58() string {
	return base58Enc(n.Xpub())
}

// Addr return btc version 1 btc public address from
// uncompressed x and y co-ordinates of public key point(p)
func (n *Node) Addr() string {
	// ref : https://en.bitcoin.it/wiki/Technical_background_of_version_1_Bitcoin_addresses

	logrus.Debugf("%X", n.k)

	pX, pY := point(n.k)
	if n.version == "public" {
		pX, pY = expand(n.k)
	}

	bX := pX.Bytes()
	bY := pY.Bytes()

	/* Pad X and Y coordinate bytes to 32-bytes */
	padX := append(bytes.Repeat([]byte{0x00}, 32-len(bX)), bX...)
	padY := append(bytes.Repeat([]byte{0x00}, 32-len(bY)), bY...)

	/* Add prefix 0x04 for uncompressed coordinates */
	p := serializeBytes([]byte{0x04}, padX, padY)

	logrus.Debugf("%X", p)

	// Perform SHA-256 hashing on the public key
	// and Perform RIPEMD-160 hashing on the result of SHA-256
	p = hash160(p)
	logrus.Debugf("after sha and ripemd \n%X", p)

	// append 0x00
	p = serializeBytes([]byte{0x00}, p)
	logrus.Debugf("add 0x00 network byte \n%X", p)

	// compress as b58check
	// get the first 4 byte from double sha256
	// added it to p
	p = serializeBytes(p, doubleSHA256(p, 4))
	logrus.Debugf("after checksum \n%X", p)

	//fmt.Printf("debug: %x \n", p)

	return base58Enc(p)
}

// CmpAddr return compress public address
func (n *Node) CmpAddr() string {
	p := []byte{}

	// for version is private
	pX, pY := point(n.k)

	if n.version == "public" {
		pX, pY = expand(n.k)
	}

	bX := pX.Bytes()

	padX := append(bytes.Repeat([]byte{0x00}, 32-len(bX)), bX...)

	/* Add prefix 0x02 or 0x03 depending on ylsb */
	if pY.Bit(0) == 0 {
		p = serializeBytes([]byte{0x02}, padX)
	} else {
		p = serializeBytes([]byte{0x03}, padX)
	}

	p = hash160(p)

	p = serializeBytes([]byte{0x00}, p)

	p = serializeBytes(p, doubleSHA256(p, 4))

	return base58Enc(p)
}
