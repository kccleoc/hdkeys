package hdkeys

import (
	"encoding/hex"
	"fmt"
	"math/big"
	"testing"
)

const (
	hChild = int64(0x80000000)
)

func TestValidation(t *testing.T) {

	key := []byte{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0}
	if err := validatePrivateKey(key); err == nil {
		t.Errorf("should have error from key1 but passed :( ")
	}

	key2 := []byte{0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2,
		3, 3, 3, 3, 3, 3, 3, 3}

	if err := validatePrivateKey(key2); err != nil {
		t.Errorf("error from key2: %s", err)
	}

	key3 := []byte{255, 255, 255, 255, 255, 255, 255, 255, 255,
		255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
		255, 255, 255, 255, 255, 255, 255, 255}

	if err := validatePrivateKey(key3); err == nil {
		t.Errorf("should have error from key3 but passed :( ")
	}

	key4 := []byte{1, 2, 3, 4, 5, 6, 7, 8, 9,
		255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
		255, 255, 255, 255, 255, 255, 255, 255, 33}

	if err := validatePrivateKey(key4); err == nil {
		t.Errorf("should have error from key3 but passed :( ")
	}
}
func TestChildNum(t *testing.T) {
	tc := []struct {
		cn    string
		value *big.Int
	}{
		{
			"2222", big.NewInt(2222),
		},
		{
			"222H", big.NewInt(222 + hChild),
		},
		{
			"22H", big.NewInt(22 + hChild),
		},
		{
			"0h", big.NewInt(0 + hChild),
		},
		{
			"2", big.NewInt(2),
		},
		{
			"2H", big.NewInt(2 + hChild),
		},
	}

	// test good things
	for _, c := range tc {
		exp, err := idxDecode(c.cn)
		if err != nil || exp.Cmp(c.value) != 0 {
			t.Errorf("%v is not expected val %v", c.value, exp)
		}
		// fmt.Println(exp)
	}

	// test bad things

	badtc := []struct {
		cn string
	}{
		{
			"22222",
		},
		{
			"A222H",
		},
		{
			"A22",
		},
		{
			"44G",
		},
		{
			"h4",
		},
		{
			"4f44",
		},
	}

	for _, c := range badtc {
		_, err := idxDecode(c.cn)
		if err == nil {
			t.Errorf("should be given error msg from (%s) but nil", c.cn)
		}
		// fmt.Println(exp)
		// fmt.Println("decoded value", c.cn, " get: ", idxDecode(c.cn))
	}
}
func TestBase58Enc(t *testing.T) {
	// https://www.browserling.com/tools/base58-encode
	stc := []struct {
		input     string
		inputForm string
		output    string
	}{
		{"CDE", "", "PbTi"},
		{"This is not good!", "", "o1JLFGZZCsrXqm4884WB7Hi"},
		{"HelloWorld", "", "54uZdajEaDdN6F"},
		{"00010966776006953d5567439e5e39f86a0d273beed61967f6",
			"hex", "16UwLL9Risc3QfPqBUvKofHmBQ7wMtjvM"},
		{"00", "hex", "1"},
		{"0099BC78BA577A95A11F1A344D4D2AE55F2F857B989EA5E5E2",
			"hex", "1F1tAaz5x1HUXrCNLbtMDqcw6o5GNn4xqX"},
	}

	bs := make([]byte, 0)
	for _, tc := range stc {
		switch tc.inputForm {
		case "":
			bs = []byte(tc.input)
		case "hex":
			bs, _ = hex.DecodeString(tc.input)
		}

		result := base58Enc(bs)
		if result != tc.output {
			t.Fatalf("expected \n%s\n to\n%s\nbut get\n%s\nInputslice:\n%v\n",
				tc.input, tc.output, result, bs)
		}
	}
}

func TestSer32(t *testing.T) {
	small := big.NewInt(1)
	fmt.Println(ser32(small))
}

func TestDecodeXprv2(t *testing.T) {
	tcs := []struct {
		xk    string
		addr  string
		caddr string
		pk    string
		cpk   string
	}{
		{"xprv9vkdwt7CJcqS9HAp6d3BrE4Zpyvpy6a8Vef4A95xUZ6dAq5rPoXBQ3Hdn" +
			"JWaMd6fAfAs4fSdEw4g6UhoA6WPeaVcAhPHs3eqSScfM6KGw7b",
			"1ExfGzt4bMivhRdQxs6w1iVN7YLD4npkTT",
			"1Mo2bAkykxn4yd7gMDdZj56hskHJBZr29m",
			"5KNQoNPwRJs8MW7fDcFt2LP2rh11BAtdSghnsUFkLnPEKdDwbUj",
			"L45W3fSFCHrFTVWPgecgCtYFCKZ1mtesrNF3TmJ6JKK7o4xpnjUJ",
		},
		{"xprv9zmECjV13hqT7mR9phgctkECKi36Bz4VDRXttgQNYBn22CyVHELQa6BPAW" +
			"6ipzRuCiiA9WBxzR3uWnm6zjp27nxeUZuNzxgte5yxiUuLEwZ",
			"1DcqdZGdTPjsS4fX8bzPnioWKZr9XMCDt7",
			"1HvUdW7v6K2QRE2SAtuZDz7mD3CG4RuqmB",
			"5JKmkkbAHjKv3Md4GmTLavn84dW4Sfq4EAJvS6FfdkzN9KzNGZt",
			"KyTrY7PmzqpV7Q29SbDFSkNAB27GCJP7F666qkBjXFACKiy1QWir",
		},
	}

	for _, tc := range tcs {
		p := RestoreXprv(tc.xk)

		// check address
		if tc.addr != p.Addr() {
			t.Errorf("pub address not match, tc %s\n ", tc.xk)
		}
		// check address uncompressed
		if tc.caddr != p.CmpAddr() {
			t.Errorf("pub address Compressed not match, tc %s\n ", tc.xk)
		}
		// check priv key
		if tc.pk != p.PriKeyWIF(false) {
			t.Errorf("pri key not match, tc %s\n ", tc.xk)
		}
		// check priv key compress
		if tc.cpk != p.PriKeyWIF(true) {
			t.Errorf("pri key compressed not match, tc %s\n ", tc.xk)
		}

	}
}

func TestGenBTCKeyPair(t *testing.T) {
	k, p := GenKeyPairs()
	t.Logf("Pri Key: %x", k)
	t.Logf("Pub Address: %s", p)
}
