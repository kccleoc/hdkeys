module "bitbucket.org/kccleoc/hdkeys"

require (
	"bitbucket.org/kccleoc/btcec" v0.0.0-20180406095258-a426601b3514
	"github.com/sirupsen/logrus" v1.0.5
	"golang.org/x/crypto" v0.0.0-20180403160946-b2aa35443fbc
	"golang.org/x/sys" v0.0.0-20180406135729-3b87a42e500a
)
